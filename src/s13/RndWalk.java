package s13;

import java.util.Random;

public class RndWalk {
	public static void main(String[] args) {
		int nbOfExperiments = 100000;
		int n = 20;
		int leftChoicePercentage = 45;
		Random r = new Random();
		System.out.printf("n=%d, leftPercentage=%d, nExperiments=%d \n", n, leftChoicePercentage, nbOfExperiments);
		System.out.println(rndWalkMirrorAvgLength(r, n, leftChoicePercentage, nbOfExperiments));
	}

	// ============================================================
	static double rndWalkMirrorAvgLength(Random r, int pointToReach, int leftChoicePercentage, int nbOfExperiments) {
		int x, nbOfSteps = 0;
		int total = 0;

		for (int i = 0; i < nbOfExperiments; i++) {
			x = 0;
			nbOfSteps = 0;

			while (x != pointToReach) {

				if (flipCoin(r, leftChoicePercentage, 100)) {
					x++;
				} else {
					x--;
				}

				if (x < 0)
					x = 0;

				nbOfSteps++;
			}

			total += nbOfSteps;
		}

		return total / (double) nbOfExperiments;

	}

	public static boolean flipCoin(Random r, int m, int n) {

		// true if m/n and false if (n-m)/n, in general nextInt(n) < m ok

		return (r.nextInt(n) < m ? true : false);
	}

}
