package s11;

import java.util.Random;

public class ObjQueue {
	// ======================================================================
	private static class ObjQueueNode {
		final Object elt;
		ObjQueueNode next = null;

		// ----------
		ObjQueueNode(Object elt) {
			this.elt = elt;
		}

	}

	// ======================================================================
	private ObjQueueNode front;
	private ObjQueueNode back;

	// ------------------------------
	public ObjQueue() {
	}

	// --------------------------
	public void enqueue(Object elt) {
		ObjQueueNode node = new ObjQueueNode(elt);

		if (isEmpty()) {
			back = node;
			front = node;
		} else {
			this.back.next = node;
			back = node;
		}

	}

	// --------------------------
	public boolean isEmpty() {
		return back == null;
	}

	// --------------------------
	// PRE : !isEmpty()
	public Object consult() {
		return front.elt;
	}

	// --------------------------
	// PRE : !isEmpty()
	public Object dequeue() {
		Object e = front.elt;
		if (front == back) {
			back = null;
			front = null;
		} else {
			front = front.next;
		}
		return e;
	}

	// --------------------------
	public String toString() {
		String res = "";
		ObjQueueNode c = front;
		while (c != null) {
			res += c.elt + " ";
			c = c.next;
		}
		return res;
	}


}
