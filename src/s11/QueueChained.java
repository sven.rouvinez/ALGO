package s11;


public class QueueChained<E> {
	// ======================================================================
	private static class QueueNode<E> {
		final E elt;
		QueueNode<E> next = null;


		// ----------
		QueueNode(E elt) {
			this.elt = elt;
		}

	}

	// ======================================================================
	private QueueNode<E> front;
	private QueueNode<E> back;

	// ------------------------------
	public QueueChained() {
	}

	// --------------------------
	public void enqueue(E elt) {
		QueueNode<E> node = new QueueNode<E>(elt);
		
		if (isEmpty()) {
			back = node;
			front = node;
		} else {
			this.back.next = node;
			back = node;
		}

	}

	// --------------------------
	public boolean isEmpty() {
		return back == null;
	}

	// --------------------------
	// PRE : !isEmpty()
	public E consult() {
		return front.elt;
	}

	// --------------------------
	// PRE : !isEmpty()
	public E dequeue() {
		E e = front.elt;
		if (front == back) {
			back = null;
			front = null;
		} else {
			front = front.next;
		}
		return e;
	}

	// --------------------------
	public String toString() {
		String res = "";
		QueueNode<E> c = front;
		while (c != null) {
			res += c.elt + " ";
			c = c.next;
		}
		return res;
	}



}
