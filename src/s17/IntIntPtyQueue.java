package s17;

import java.util.Random;
import s11.IntQueueChained;

// ------------------------------------------------------------
public class IntIntPtyQueue {
	private final IntQueueChained[] fifoQueues;
	private final int nbPtyLevels;

	// can be used to reduce the complexityx
	private int highesthPty;

	// or: IntQueueArray [] qt;
	// ...
	// ------------------------------------------------------------
	// priorities will be in [0 .. nPriorities-1] 0 is the hightest
	public IntIntPtyQueue(int nPriorities) {
		this.fifoQueues = new IntQueueChained[nPriorities];
		this.nbPtyLevels = nPriorities;

		for (int i = 0; i < fifoQueues.length; i++)
			fifoQueues[i] = new IntQueueChained();

	}

	// ------------------------------------------------------------
	public boolean isEmpty() {

		for (IntQueueChained f : fifoQueues)
			if (!f.isEmpty())
				return false;

		return true;
	}

	// ------------------------------------------------------------
	// PRE: 0 <= pty < nPriorities
	public void enqueue(int elt, int pty) {
		fifoQueues[pty].enqueue(elt);
	}

	// ------------------------------------------------------------
	// strongest pty present in the queue.
	// PRE: ! isEmpty()
	public int consultPty() {
		for (int i = 0; i < fifoQueues.length; i++)
			if (!fifoQueues[i].isEmpty())
				return i;

		return -1;
	}

	// ------------------------------------------------------------
	// elt with strongest (=smallest) pty.
	// PRE: ! isEmpty()
	public int consult() {
		for (int i = 0; i < fifoQueues.length; i++)
			if (!fifoQueues[i].isEmpty())
				return fifoQueues[i].consult();

		return -1;
	}

	// ------------------------------------------------------------
	// elt with strongest (=smallest) pty.
	// PRE: ! isEmpty()
	public int dequeue() {

		for (int i = 0; i < fifoQueues.length; i++)
			if (!fifoQueues[i].isEmpty())
				return fifoQueues[i].dequeue();

		return -1;
	}

	// ------------------------------------------------------------
	// ------------------------------------------------------------
	// ------------------------------------------------------------
	public static void main(String[] args) {
		Random r = new Random();
		long seed = r.nextInt(1000);
		r.setSeed(seed);
		System.out.println("Using seed " + seed);
		int n = 10000;
		if (args.length == 1)
			n = Integer.parseInt(args[0]);
		int p, e;
		IntIntPtyQueue pq = new IntIntPtyQueue(n);
		for (int i = 0; i < 10 * n; i++) {
			p = r.nextInt(n);
			pq.enqueue(p, p);
		}
		e = Integer.MIN_VALUE;
		for (int i = 0; i < 10 * n; i++) {
			p = pq.dequeue();
			ok(p >= e);
			e = p;
		}
		ok(pq.isEmpty());
		for (int i = 0; i < 10 * n; i++) {
			p = r.nextInt(n);
			pq.enqueue(p, p);
			p = r.nextInt(n);
			pq.enqueue(p, p);
			pq.dequeue();
		}
		e = Integer.MIN_VALUE;
		while (!pq.isEmpty()) {
			p = pq.dequeue();
			ok(p >= e);
			e = p;
		}
		System.out.println("Test passed successfully");
	}

	// ------------------------------------------------------------
	static void ok(boolean b) {
		if (b)
			return;
		throw new RuntimeException("property not verified: ");
	}
}
