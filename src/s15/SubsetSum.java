package s15;

import java.util.Arrays;

public class SubsetSum {
	// ============================================================
	public static void main(String[] args) {
		int[] t = { 5, 8, 3 };
		int max = 20;
		System.out.println(Arrays.toString(t));
		System.out.println("Valid sums < " + max);
		for (int i = 0; i < max; i++) {
			boolean r = subsetSumDyn(t, i);
			if (r)
				System.out.print(i + " ");
		}
	}

	// ============================================================
	// Subset Sum Problem : Dynamic Programming version
	// ============================================================
	public static boolean subsetSumDyn(int[] t, int k0) {

		boolean[] reachableSums = new boolean[k0 + 1];
		reachableSums[0] = true;

		for (int i = 0; i < t.length; i++) {

			for (int j = reachableSums.length - 1; j >= 0; j--) {
				if (reachableSums[j] & j + t[i] < reachableSums.length)
					reachableSums[j + t[i]] = true;

			}
		}
		return reachableSums[reachableSums.length - 1];
	}

}
