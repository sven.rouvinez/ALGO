package s15;

public class GridPath {
	// ============================================================
	// Min Path Problem : Recursive version
	// ============================================================
	public static int minPath(int[][] t) {
		return minPath(t, t.length - 1, t[0].length - 1);
	}

	public static int minPath(int[][] t, int i, int j) {
		if (i < 0 || j < 0)
			return Integer.MAX_VALUE / 2;
		if (i == 0 && j == 0)
			return t[0][0];
		int a = minPath(t, i - 1, j);
		int b = minPath(t, i, j - 1);
		if (b < a)
			a = b;
		return a + t[i][j];
	}

	// ============================================================
	// Min Path Problem : Dynamic Programming version
	// ============================================================

	public static int minPathDyn(int[][] t) {
		return minPathDyn(t, t.length - 1, t[0].length - 1);
	}

	public static int minPathDyn(int[][] t, int iz, int jz) {
		int[][] minPathSol = new int[t.length][t[0].length];
		minPathSol[0][0] = t[0][0];

		for (int i = 0; i <= iz; i++) {

			for (int j = 0; j <= jz; j++) {

				if (i == 0 && j == 0) {
					continue;
					// first row
				} else if (i == 0 && j > 0) {
					minPathSol[i][j] = minPathSol[i][j - 1] + t[i][j];
					// first column
				} else if (j == 0 && i > 0) {
					minPathSol[i][j] = minPathSol[i - 1][j] + t[i][j];
				} else {
					if (minPathSol[i - 1][j] < minPathSol[i][j - 1]) {
						minPathSol[i][j] = minPathSol[i - 1][j] + t[i][j];
					} else {
						minPathSol[i][j] = minPathSol[i][j - 1] + t[i][j];
					}
				}
			}
		}

		for (int i = 0; i < minPathSol.length; i++) {
			System.out.println("");
			for (int j = 0; j < minPathSol[i].length; j++) {
				System.out.print(minPathSol[i][j] + " | ");
			}
		}
		System.out.println();

		return minPathSol[iz][jz];
	}

	// ============================================================
	// Small Main
	// ============================================================
	public static void main(String[] args) {
		int[][] t = { { 2, 2, 6, 7 }, { 3, 8, 5, 9 }, { 2, 1, 8, 2 }, { 4, 2, 3, 5 } };
		System.out.println(minPath(t));
		System.out.println(minPathDyn(t));
	}
}
