package s06;
public class ShortToStringMapItr {
	private int index;
	private ShortToStringMap m;

  // ----------------------------------------
 public ShortToStringMapItr (ShortToStringMap m) {
		this.m = m;
  }
 // ----------------------------------------
  public boolean hasMoreKeys() {
		return index < m.size();
  }
  // ----------------------------------------
  // PRE-condition: hasMoreKeys()
	public short nextKey() {

		return m.keys[index++];

  }
}
