package s06;

import java.util.Arrays;

public class ShortToStringMap {
	private int size;
	protected short[] keys;
	protected String[] values;

	// TODO - A COMPLETER...
	// ------------------------------
	// Private methods
	// ------------------------------

	// Could be useful, for instance :
	// - one method to detect and handle the "array is full" situation
	// - one method to locate a key in the array
	// (to be called from containsKey(), put(), and remove())

	// ------------------------------
	// Public methods
	// ------------------------------

	// ------------------------------------------------------------
	public ShortToStringMap() {
		final int MAX_VALUE = 30;
		this.keys = new short[MAX_VALUE];
		this.values = new String[MAX_VALUE];
	}

	// ------------------------------------------------------------
	// adds an entry in the map, or updates it
	public void put(short key, String img) {

		if (containsKey(key)) {
			values[locateKey(key)] = img;
		} else {
			if (keys.length == size())
				updateSize();

			keys[size] = key;
			values[size] = img;
			size++;
		}

	}
    //the fonction to extend the size  of the table
	private void updateSize() {
		keys = Arrays.copyOf(keys, size * 2);
		values = Arrays.copyOf(values, size * 2);
	}

    //the method to locate the id of the key.
	private int locateKey(short key) {

		for (int i = 0; i < size; ++i) {
			if (keys[i] == key) {
				return i;
			}
		}

		return -1;
	}

	// ------------------------------------------------------------
	// returns null if !containsKey(key)
	public String get(short key) {

		if (!containsKey(key))
			return null;

		return values[locateKey(key)];
	}

	// ------------------------------------------------------------
	//the method to remove a inscriptionin the table
	public void remove(short e) {
		int indexKey = locateKey(e);

		if (keys.length == size())
			updateSize();

		if (indexKey > -1) {
			keys[indexKey] = keys[size - 1];
			values[indexKey] = values[size - 1];
			size--;
		}
	}

	// ------------------------------------------------------------
	public boolean containsKey(short k) {
		return locateKey(k) > -1;
	}

	// ------------------------------------------------------------
	public boolean isEmpty() {
		return size() == 0;
	}

	public int size() {
		return size;
	}

	// ------------------------------------------------------------
	// a.union(b) : a becomes "a union b"
	// values are those in b whenever possible
	public void union(ShortToStringMap m) {
		for (short i = 0; i < m.size(); ++i) {
			this.put(m.keys[i], m.values[i]);
		}

	}

	// ------------------------------------------------------------
	// a.intersection(b) : "a becomes a intersection b"
	// values are those in b
	public void intersection(ShortToStringMap s) {

		for (short i = 0; i < this.size; i++) {

			if (!s.containsKey(this.keys[i])) {
				this.remove(this.keys[i]);
				i = -1;

			} else {
				this.put(this.keys[i], s.get(this.keys[i]));
			}

		}

	}

	// ------------------------------------------------------------
	// a.toString() returns all elements in
	// a string like: {3:"abc",9:"xy",-5:"jk"}
	public String toString() {
		StringBuilder str = new StringBuilder();

		str.append("{");

		for (short i = 0; i < size; i++) {
			str.append(keys[i] + ":" + "\"" + values[i] + "\"" + " ");
		}
		str.append("}");

		return str.toString();
	}

	public static void main(String[] args) {
		ShortToStringMap test = new ShortToStringMap();
		ShortToStringMap test2 = new ShortToStringMap();
		short[] s1 = { 1, 6, 4, 32, 8, 9, 1, 6, 4, 32, 8, 9, 0 };
		short[] s2 = { 4, 2, 9, 32, 7, 6, 5, 34, 4, 2, 0 };

		// test.put((short) 20, "j");
		for (short s : s1) {
			test.put(s, "test");
		}

		for (short s : s2) {
			test2.put(s, "test2");
		}

		// test2.put((short) 8, "j");
		test2.put((short) 4, "tefsdfsd");

		System.out.println(test);
		System.out.println(test2);
		test.intersection(test2);
		System.out.println("After");
		System.out.println(test);
		System.out.println(test2);

	}

}