package s16;

import java.util.Random;

import javax.swing.plaf.synth.SynthSeparatorUI;

import java.util.Queue;
import java.util.LinkedList;

public class ExoBTree {
	// ------------------------------------------------------------
	public static String breadthFirstQ(BTNode t) {
		String res = "";
		Queue<BTNode> q = new LinkedList<BTNode>();
		q.add(t);
		while (!q.isEmpty()) {
			BTNode crt = q.remove();
			if (crt == null)
				continue;
			res += (" " + crt.elt);
			q.add(crt.left);
			q.add(crt.right);
		}
		return res;
	}

	// ------------------------------------------------------------
	public static int size(BTNode t) {
		if (t == null)
			return 0;

		return 1 + size(t.left) + size(t.right);
	}

	// ------------------------------------------------------------
	public static int height(BTNode t) {
		if (t == null)
			return 0;

		int hR = 1 + height(t.right);
		int hL = 1 + height(t.left);

		return hR >= hL ? hR : hL;
	}

	// ------------------------------------------------------------
	public static String breadthFirstR(BTNode t) {
		String str = new String();

		for (int i = 0; i <= height(t); i++)
			str += visitLevel(t, i);

		return str;
	}

	// ------------------------------------------------------------
	public static String visitLevel(BTNode t, int level) {

		if (t == null)
			return "";

		if (level == 1)
			return t.elt + " ";

		return visitLevel(t.left, level - 1) + visitLevel(t.right, level - 1);
	}

	// ------------------------------------------------------------
	public static void depthFirst(BTNode t) {
		if (t == null)
			return;
		System.out.print(" " + t.elt);
		depthFirst(t.left);
		depthFirst(t.right);
	}

	// ------------------------------------------------------------
	// PRE: y.left != null
	public static void rotateRight(BTNode y) {
		// store the old node
		BTNode x = y.left;

		BTNode parent = y.parent;

		// if there's a root
		if (parent != null) {

			if (parent.right == y) {
				parent.right = x;
			}
			if (parent.left == y) {
				parent.left = x;
			}
		}

		// x is the new root
		x.parent = parent;
		y.parent = x;
		y.left = x.right;

		if (x.right != null)
			x.right.parent = y;

		x.right = y;

	}

	// ------------------------------------------------------------
	// ------------------------------------------------------------
	// ------------------------------------------------------------
	private static Random rnd = new Random();

	// ------------------------------------------------------------
	public static BTNode rndTree(int size) {
		if (size == 0)
			return null;
		BTNode root = new BTNode(new Integer(0), null, null, null);
		BTNode t = root;
		boolean isLeft;
		for (int i = 1; i < size; i++) {
			t = root;
			while (true) {
				isLeft = rnd.nextBoolean();
				if (isLeft) {
					if (t.left == null)
						break;
					t = t.left;
				} else {
					if (t.right == null)
						break;
					t = t.right;
				}
			}
			BTNode newLeaf = new BTNode(new Integer(i), null, null, t);
			if (isLeft)
				t.left = newLeaf;
			else
				t.right = newLeaf;
		}
		return root;
	}

	// ------------------------------------------------------------
	public static void main(String[] args) {
		int nbOfNodes = 10;
		BTNode t = rndTree(nbOfNodes);
		System.out.println("Tree:" + t);
		System.out.println(t.toReadableString());
		System.out.println("\nDepth first (recursive), preorder:");
		depthFirst(t);
		System.out.println("\nBreadth first:");
		System.out.println(breadthFirstQ(t));
		System.out.println("\nBreadth first bis:");
		System.out.println(breadthFirstR(t));
		System.out.println("\nSize:" + size(t));
		System.out.println("\nHeight:" + height(t));
		System.out.println("\nElements by level: " + visitLevel(t, 2));

	}
	// ------------------------------------------------------------
}
