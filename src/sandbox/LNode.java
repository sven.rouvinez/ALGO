package sandbox;

public class LNode {
	int elt;
	LNode next;

	public LNode(int elt, LNode next) {
		this.elt = elt;
		this.next = next;
	}

	public static void removeRepetitions(LNode head) {
		while (head.next != null) {
			if (head.elt == head.next.elt) {
				head.next = head.next.next;
			}
		}
	}

	public static String printList(LNode head) {
		if (head.next == null)
			return Integer.toString(head.elt);

		return printList(head.next);
	}

	public static void main(String[] args) {
		LNode n6 = new LNode(5, null);
		LNode n5 = new LNode(4, n6);
		LNode n4 = new LNode(3, n5);
		LNode n3 = new LNode(3, n4);
		LNode n2 = new LNode(2, n3);
		LNode n1 = new LNode(2, n2);


		System.out.println(printList(n1));
		removeRepetitions(n1);

	}
}
