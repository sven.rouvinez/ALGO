package s08;



public class RLExos {
	// ----------------------------------------------------------------
	// --- Exercices, S�rie 8 :
	// ----------------------------------------------------------------
	public static CharRecList append(CharRecList l, char e) {

		if (l.isEmpty())
			return l.withHead(e);


		return append(l.tail(), e).withHead(l.head());
	}

	public static CharRecList removeRepetitions(CharRecList l) {
		if (l.isEmpty() || l.tail().isEmpty())
			return l;

		if (l.head() != l.tail().head())
			return removeRepetitions(l.tail()).withHead(l.head());

		return removeRepetitions(l.tail());
	}

	public static CharRecList placeHead(CharRecList l) {

		if (l.tail().isEmpty())
			return placeHead(l.tail()).withHead(l.head());


		return placeHead(l.tail());
	}

	// ----------------------------------------------------------------
	public static CharRecList concat(CharRecList l, CharRecList r) {
		if (l.isEmpty())
			return r;

		return concat(l.tail(), r).withHead(l.head());
	}

	// ----------------------------------------------------------------
	public static CharRecList replaceEach(CharRecList l, char old, char by) {
		if (l.isEmpty())
			return l;

		if (l.head() == old)
			return replaceEach(l.tail(), old, by).withHead(by);

		return replaceEach(l.tail(), old, by).withHead(l.head());
	}

	// ----------------------------------------------------------------
	public static char consultAt(CharRecList l, int index) {
		// pre-condition: l can't be empty

		if (index == 0)
			return l.head();

		return consultAt(l.tail(), index - 1);
	}

	// ----------------------------------------------------------------
	public static boolean isEqual(CharRecList l, CharRecList o) {

		if (l.isEmpty() || o.isEmpty())
			return l.isEmpty() && o.isEmpty();

		if (l.head() != o.head())
			return false;

		return isEqual(l.tail(), o.tail());
	}


	public static boolean isSuffix(CharRecList l, CharRecList suff) {
		if (suff.isEmpty())
			return true;

		if (l.isEmpty())
			return false;

		if (isEqual(l, suff))
			return true;

		return isSuffix(l.tail(), suff);

	}

	public static CharRecList z(CharRecList l, int i) {
		if (i == 0)
			return CharRecList.EMPTY_LIST;
		if (l.isEmpty())
			return l;
		return z(l.tail(), i - 1).withHead(l.head());
	}

	// ----------------------------------------------------------------
	// --- Exemples du cours :
	// ----------------------------------------------------------------
	public static int sizeOf(CharRecList l) {
		if (l.isEmpty())
			return 0;
		return 1 + sizeOf(l.tail());
	}

	// ---------------------------------------------------------------
	public static CharRecList inverse(CharRecList l) {
		if (l.isEmpty())
			return l;
		return append(inverse(l.tail()), l.head());
	}

	// ----------------------------------------------------------------
	public static boolean isMember(CharRecList l, char e) {
		if (l.isEmpty())
			return false;
		if (e == l.head())
			return true;
		return isMember(l.tail(), e);
	}

	// ----------------------------------------------------------------
	public static CharRecList smaller(CharRecList l, char e) {
		if (l.isEmpty())
			return l;
		if (l.head() < e)
			return smaller(l.tail(), e).withHead(l.head());
		return smaller(l.tail(), e);
	}

	// ----------------------------------------------------------------
	public static CharRecList greaterOrEqual(CharRecList l, char e) {
		if (l.isEmpty())
			return l;
		if (l.head() < e)
			return greaterOrEqual(l.tail(), e);
		return greaterOrEqual(l.tail(), e).withHead(l.head());
	}

	// ----------------------------------------------------------------
	public static CharRecList quickSort(CharRecList l) {
		CharRecList left, right;
		if (l.isEmpty())
			return l;
		left = smaller(l.tail(), l.head());
		right = greaterOrEqual(l.tail(), l.head());
		left = quickSort(left);
		right = quickSort(right);
		return concat(left, right.withHead(l.head()));
	}

	// ----------------------------------------------------------------
	// ----------------------------------------------------------------
	// ----------------------------------------------------------------
	public static void main(String[] args) {
		CharRecList l = CharRecList.EMPTY_LIST;
		CharRecList m = CharRecList.EMPTY_LIST;
		CharRecList o = CharRecList.EMPTY_LIST;

		l = l.withHead('c').withHead('d').withHead('a').withHead('b').withHead('z').withHead('4');
		m = m.withHead('t').withHead('u').withHead('v');
		CharRecList p = CharRecList.EMPTY_LIST;
		p = p.withHead('a').withHead('a').withHead('e').withHead('f').withHead('z').withHead('z').withHead('z')
				.withHead('z').withHead('x');
		CharRecList q = CharRecList.EMPTY_LIST;
		q = q.withHead('d').withHead('c').withHead('b').withHead('a');

		// o = o.withHead('c').withHead('d').withHead('a');
		o = o.withHead('d').withHead('a');
		// o =
		// o.withHead('c').withHead('d').withHead('a').withHead('b').withHead('z').withHead('4');

		System.out.println("list l : " + l);
		System.out.println("list m : " + m);
		System.out.println("list o : " + o);
		System.out.println("list p : " + p);
		System.out.println("list q : " + q);
		System.out.println("Replace head: " + placeHead(q));
		System.out.println("Repetition: " + removeRepetitions(p));
		System.out.println("quickSort(l) : " + quickSort(l));
		System.out.println("append(l,'z') : " + append(l, 'z'));
		System.out.println("concat(l,m) : " + concat(l, m));
		System.out.println("replaceEach(l,'a','z') : " + replaceEach(l, 'a', 'z'));
		System.out.println("consultAt(l,2) : " + consultAt(l, 4));
		System.out.println("Is l == m: " + isEqual(l, o));
		System.out.println("Is l is suffix o: " + isSuffix(l, o));
		System.out.println("fct z: " + z(l, 2));

		// ...
	}

}
