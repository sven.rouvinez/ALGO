package s12;
public class BitSetOfShortsItr {
	final BitSetOfShorts bs;
	int index = 0;
	// ------------------------- -----------------------------------
  public BitSetOfShortsItr (BitSetOfShorts theSet) {
		this.bs = theSet;
  }

  public boolean hasMoreElements() {
		return bs.size() < index;
  } 

	public short nextElement() {
		return BitSetOfShorts.eltFromIndex(index++);
  } 

}
