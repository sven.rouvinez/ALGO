package s04;

public class Sorting {
	public static void main(String[] args) {
		int[] t = { 4, 3, 2, 6, 8, 7 };
		int[] u = { 2, 3, 4, 6, 7, 8 };
		int[] test = { 13, 11, 7, 16, 2, 1, 8, 14, 18, 20, 9, 10, 12, 4, 3, 6, 15, 5, 17, 19 };
		int[] testCorr = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };

		// Algo testing
		shellSort(test);
		// selectionSort(test);
		// insertionSort(test);

		for (int i = 0; i < t.length; i++)
			if (test[i] != testCorr[i]) {
				System.out.println("Something is wrong...");
				return;
			}
		int[] v = { 5 };
		insertionSort(v);
		int[] w = {};
		insertionSort(w);
		System.out.println("\nMini-test passed successfully...");
	}

	// ------------------------------------------------------------
	public static void selectionSort(int[] a) {

		int tmp;

		for (int i = 0; i < a.length; ++i) {
			int minIndex = minValueIndex(i, a);

			// get the min element of the array
			if (a[minIndex] != a[i]) {
				tmp = a[i];
				// swap the actual value with min value index in array
				a[i] = a[minIndex];
				a[minIndex] = tmp;
			}
		}
	}

	// return the index of minimal element from the actual index
	public static int minValueIndex(int actualIndex, int[] a) {
		int valueIndexMin = actualIndex;

		for (; actualIndex < a.length; ++actualIndex) {
			if (a[valueIndexMin] > a[actualIndex])
				valueIndexMin = actualIndex;
		}
		return valueIndexMin;
	}

	// ------------------------------------------------------------
	public static void shellSort(int[] a) {
		int k = 6;
		k = 1 + 3 * k;
		for (; k > 0; k--) {
			insertionSortWithK(a, k);
		}
	}

	public static void insertionSortWithK(int[] a, int k) {
		int i, j, v;

		for (i = 1; i < a.length; i += k) {
			v = a[i]; // v is the element to insert
			j = i;
			while (j > 0 && a[j - 1] > v) {
				a[j] = a[j - 1]; // move to the right
				j--;
			}
			a[j] = v; // insert the element
		}
	}

	// ------------------------------------------------------------
	public static void insertionSort(int[] a) {
		int i, j, v;

		for (i = 1; i < a.length; i++) {
			v = a[i]; // v is the element to insert
			j = i;
			while (j > 0 && a[j - 1] > v) {
				a[j] = a[j - 1]; // move to the right
				j--;
			}
			a[j] = v; // insert the element
		}
	}
	// ------------------------------------------------------------
}
