
package s07;

public class Quicksort {
	public static void main(String[] args) {
		int[] t = { 4, 3, 2, 6, 8, 7 };
		int[] u = { 2, 3, 4, 6, 7, 8 };
		quickSort(t);
		for (int i = 0; i < t.length; i++)
			if (t[i] != u[i]) {
				System.out.println("Oups. Something is wrong...");
				System.exit(-1);
			}
		System.out.println("OK. Tiny test passed...");
	}

	// ------------------------------------------------------------
	private static int partition(int[] t, int left, int right) {

		// set the pivot, first element of partition
		int pivot = t[left];

		// set the first k on pivot
		int k = left;
		int tmp = 0;

		// start with index + 1
		for (int i = left + 1; i <= right; ++i) {

			// swap between k and i and set the new k
			if (t[i] <= pivot) {
				tmp = t[k + 1];
				t[k + 1] = t[i];
				t[i] = tmp;
				k = i;
			}

			// move the pivot between pivot <= and pivot >
			if (i == right) {
				tmp = pivot;
				t[left] = t[k];
				t[k] = tmp;
			}

		}
		// return the index of pivot
		return k;
	}

	// ------------------------------------------------------------
	private static void quickSort(int[] t, int left, int right) {
		if (left > right)
			return;
		int p = partition(t, left, right);
		quickSort(t, left, p - 1);
		quickSort(t, p + 1, right);
	}

	// ------------------------------------------------------------
	public static void quickSort(int[] t) {
		quickSort(t, 0, t.length - 1);
	}
}
