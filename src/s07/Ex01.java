package s07;


import java.util.Arrays;

public class Ex01 {

	public static void main(String[] args) {
		int[] a = { 4, 6, 1, 3, 2, 5 };
		System.out.println("Smallest element in 4, 6, 1, 3, 2, 5 is " + oneRecursiveSort(a, 0) + " with one recursive");
		System.out.println("Smallest element in 4, 6, 1, 3, 2, 5 is " + divideConquerSort(a) + " with two recursive");

	}

	public static int oneRecursiveSort(int a[], int i) {

		// at the end of tab return last element is the min
		if (i == a.length - 1)
			return a[i];


		int value = oneRecursiveSort(a, i + 1);
		return a[i] < value ? a[i] : value;
	}

	public static int divideConquerSort(int a[]) {
		// find the mid tab
		int mid = a.length / 2;

		// copy from mid to end of a
		int[] b = Arrays.copyOfRange(a, mid, a.length);

		// copy from start to mid
		a = Arrays.copyOfRange(a, 0, mid);

		int minVa = oneRecursiveSort(a, 0);
		int minVb = oneRecursiveSort(b, 0);

		// return minValue between arrays
		return minVa < minVb ? minVa : minVb;

	}

}
