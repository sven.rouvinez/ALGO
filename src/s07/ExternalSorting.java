package s07;

import java.io.*;

public class ExternalSorting {
	public static void main(String[] args) {
		String filename = "src/s07/myFile.txt";
		if (args.length > 0)
			filename = args[0];
		mergeSort2(filename);
	}

	// ------------------------------------------------------------
	// ------------------------------------------------------------
	private static boolean isMonotone(String crt, String prev) {
		if (crt == null)
			return false;
		if (prev == null)
			return true;
		return (crt.compareTo(prev) >= 0);
	}

	// ------------------------------------------------------------
	private static void merge(String a, String b, String c) throws IOException {
		BufferedReader fa = new BufferedReader(new FileReader(a));
		BufferedReader fb = new BufferedReader(new FileReader(b));
		PrintWriter fc = new PrintWriter(new FileWriter(c));
		String sa = fa.readLine();
		String saPrev = null;
		String sb = fb.readLine();
		String sbPrev = null;
		while (sa != null || sb != null) {
			// if needed, go to the next two monotone sequences
			if (!isMonotone(sa, saPrev) && !isMonotone(sb, sbPrev)) {
				saPrev = sa;
				sbPrev = sb;
			}
			if (!isMonotone(sb, sbPrev) || isMonotone(sa, saPrev) && sa.compareTo(sb) <= 0) {
				fc.println(sa);
				saPrev = sa;
				sa = fa.readLine();
			} else {
				fc.println(sb);
				sbPrev = sb;
				sb = fb.readLine();
			}
		}
		fa.close();
		fb.close();
		fc.close();
	}

	// ------------------------------------------------------------
	private static int split(String a, String b, String c) throws IOException {
		BufferedReader fa = new BufferedReader(new FileReader(a));
		PrintWriter fb = new PrintWriter(new FileWriter(b));
		PrintWriter fc = new PrintWriter(new FileWriter(c));

		String line;
		String prev = fa.readLine();

		// start monotone at 1 because even if there's one line monotone is 1
		int nMonot = 1;

		// use for to alternate writing between 2 temp files
		boolean writeFileB = true;

		// write firstline in b
		if (prev != null)
			fb.println(prev);


		while ((line = fa.readLine()) != null) {

			// count the monotone and change writeFileB
			if (!isMonotone(line, prev)) {
				writeFileB = !writeFileB;
				nMonot++;
			}

			if (writeFileB) {
				fb.println(line);

			} else {
				fc.println(line);

			}

		}

		fb.close();
		fa.close();
		fc.close();

		return nMonot;
	}

	// ------------------------------------------------------------
	public static void mergeSort2(String filename) {
		String tmp1 = filename + ".tmp1"; // somewhat...
		String tmp2 = filename + ".tmp2"; // ...dangerous...
		int nMonotonies;
		try {
			nMonotonies = split(filename, tmp1, tmp2);
			while (nMonotonies > 1) {
				merge(tmp1, tmp2, filename);
				nMonotonies = split(filename, tmp1, tmp2);
			}
		} catch (IOException e) {
			System.out.println(e);
		}
	}
	// ------------------------------------------------------------
}
