package s01;

public class Parenth {
	// -------------------------------------
	// Usage: java Parenth 'aa(({adf}))' don't forget the ' !
	// -------------------------------------
	public static void main(String[] args) {
		if (args.length != 0) {
			String s = args[0];
			System.out.println(s + " : " + isBalanced(s));
		}
		String[] t = { "((o{()oo})o)", "oo((((((((((((((((((((o))))))))))))))))))))o{}", "oo())(()", "oo()((())()",
				"oo()((()})", ")()", "((}))" };
		boolean[] answer = { true, true, false, false, false, false, false };
		boolean ok = true;
		for (int i = 0; i < t.length; i++) {
			ok = ok & (isBalanced(t[i]) == answer[i]);
			System.out.print(t[i] + " : " + isBalanced(t[i]));
			System.out.println(" (should be " + answer[i] + ")");
		}
		if (ok)
			System.out.println("\nTest passed successfully");
		else
			System.out.println("\nOups... There's a bug !");
	}

	// -------------------------------------
	public static boolean isBalanced(String l) {
		char c;
		CharStack s = new CharStack();

		for (int i = 0; i < l.length(); i++) {
			c = l.charAt(i);
            //check the parenthese (c) and put it in the stack s
			if (isOpeningParenth(c))
				s.push(c);
            //check to see a closingparenthese (c)
			if (isClosingParenth(c)) {
			    //compare the actual closingparent with the last one in the stack s, and then remove it from the stack
				if (!isMatchingParenth(s.pop(), c)) {
				    //if there is too much closingparent, then it's not matching anymore and return a false
					return false;
				}
			}

		}
		//if there is too much opening parenth, the stack s will not be empty at the end and the we return a false as well
		if (!s.isEmpty())
			return false;
		//if all matched and the s stack is empty then we return truc because it's balanced
		return true;
	}

	// -------------------------------------
	private static boolean isOpeningParenth(char c) {
		return (c == '(') || (c == '{');
	}

	private static boolean isClosingParenth(char c) {
		return (c == ')') || (c == '}');
	}

	private static boolean isMatchingParenth(char c1, char c2) {
		return ((c1 == '(') && (c2 == ')')) || ((c1 == '{') && (c2 == '}'));
	}
}
