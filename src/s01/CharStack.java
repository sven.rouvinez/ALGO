package s01;

public class CharStack {
	private int topIndex;
	private char[] buffer;
	// -------------------------------------
	private static final int DEFAULT_SIZE = 10;

	// -------------------------------------
	public CharStack() {
		this(DEFAULT_SIZE);
	}

	// -------------------------------------
	public CharStack(int estimatedSize) {
	    //if the number is under or equal 0, we take the default size
		if(estimatedSize <= 0) estimatedSize=DEFAULT_SIZE;
		//we create a buffer
		this.buffer = new char[estimatedSize];
		this.topIndex = 0;
	}

	// -------------------------------------

	
	public boolean isEmpty()   {
	 return topIndex<=0;
  } 
  

	// -------------------------------------
	public char top() {
		assert !isEmpty();
	    //give back the last index in the buffer
		return buffer[topIndex];
	}

	// -------------------------------------
	public char pop() {
		assert !isEmpty();
	    //if the buffer is empty
		if (topIndex <= 0)
			return '\0';
        //save the actual index in a variable
		char poped = buffer[topIndex];
		//delete the actual variable and then decrement the Index of 1
		buffer[topIndex--] = '\u0000';
		return poped;
	}

	// -------------------------------------
	public void push(char x) {
	    //if the buffer is full then we create a other table doublesize
		if (buffer[buffer.length - 1] != '\u0000')
			resize();
		//we put the variable x at the actual index	
		buffer[++topIndex] = x;
	}
	// -------------------------------------

	private void resize() {
//create a table double size
		char[] newBuffer = new char[buffer.length * 2];
//copy from the old to the new one
		for (int i = 0; i < buffer.length; ++i) {
			newBuffer[i] = buffer[i];
		}
		this.buffer = newBuffer;

	}
    //to test the program
	public static void main(String[] args) {
		CharStack buff = new CharStack(2);
		buff.pop();
		buff.push('1');
		buff.push('2');
		buff.push('3');
		buff.push('5');
		System.out.println("Must be 5 "+ "State: "+buff.top());

		buff.pop();
		buff.pop();
		System.out.println("Must be 2 "+ "State: "+buff.top());
		
		buff.push('d');
		System.out.println("Must be false "+ "State: "+buff.isEmpty());
		System.out.println("Must be d "+ "State: "+buff.top());

	}
}
