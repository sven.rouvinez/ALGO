package s03;

public class AmStramGram {
	public static void main(String[] args) {
		int n = 523, k = 11;
		if (args.length == 2) {
			n = Integer.parseInt(args[0]);
			k = Integer.parseInt(args[1]);
		}
		System.out.println("Winner is " + winnerAmStramGram(n, k));
	}

	public static int winnerAmStramGram(int n, int k) {
		List l = new List();
		ListItr li = new ListItr(l);
		int i;

		// generate list 1..n
		while (n > 0)
			li.insertAfter(n--);


		while (l.size > 1) {

			// iterate to k
			for (i = 1; i < k; ++i) {
				li.goToNext();

				if (li.isLast()) {
					li.goToFirst();
				}
			}

			li.removeAfter();

			// if remove was the last one go to first element
			if (li.isLast())
				li.goToFirst();

		}

		return li.consultAfter();
	}
	// ----------------------------------------------------------
}
