package s03;

class ListNode {
	int elt;
	ListNode next, prev;

	public ListNode(int theElement, ListNode thePrev, ListNode theNext) {
		this.elt = theElement;
		this.prev = thePrev;
		this.next = theNext;
	}
}
