package s03;

public class ListItr {
	final List list;
	ListNode pred, succ;

	// ----------------------------------------------------------
	public ListItr(List anyList) {
		list = anyList;
		goToFirst();
	}

	// ----------------------------------------------------------
	public void insertAfter(int e) {
		// create a new node
		ListNode newNode = new ListNode(e, null, null);

		// first case
		if (list.isEmpty()) {
			newNode = new ListNode(e, null, null);
			// the element is first and last
			list.first = newNode;
			list.last = newNode;

		} else if (isLast()) {
			// no next element because it's last
			newNode = new ListNode(e, pred, null);
			pred.next = newNode;
			list.last = newNode;

		} else if (isFirst()) {
			// no pred element because it's first
			newNode = new ListNode(e, null, succ);
			succ.prev = newNode;
			list.first = newNode;

		} else {
			// normal case between two elements
			newNode = new ListNode(e, pred, succ);
			pred.next = newNode;
			succ.prev = newNode;
		}

		succ = newNode;
		list.size++;
	}

	// ----------------------------------------------------------
	public void removeAfter() {

		if (isFirst()) {
			list.first = succ.next;
			succ = list.first;

			// test if the element is first and last
			if (isLast()) {
				list.last = null;
			} else {
				succ.prev = null;
			}
		} else if (succ.next == null) {
			pred.next = null;
			succ = null;
			list.last = pred;
		} else {
			succ = succ.next;
			pred.next = succ;
			succ.prev = pred;

		}

		list.size--;
	}

	// ----------------------------------------------------------
	public int consultAfter() {
		return succ.elt;
	}

	public void goToNext() {
		pred = succ;
		succ = succ.next;
	}

	public void goToPrev() {
		succ = pred;
		pred = pred.prev;
	}

	public void goToFirst() {
		succ = list.first;
		pred = null;
	}

	public void goToLast() {
		pred = list.last;
		succ = null;
	}

	public boolean isFirst() {
		return pred == null;
	}

	public boolean isLast() {
		return succ == null;
	}
}

// When isFirst(), it is forbidden to call goToPrev()
// When isLast(), it is forbidden to call goToNext()
// When isLast(), it is forbidden to call consultAfter(), or removeAfter()
// For an empty list, isLast()==isFirst()==true
// For a fresh ListItr, isFirst()==true
// Using multiple iterators on the same list is allowed only
// if none of them modifies the list
