package s14;

public class SetOfStringsItr {
	final SetOfStrings set;
	int index;

	// ------------------------------------------------------------
	public SetOfStringsItr(SetOfStrings theSet) {
		this.set = theSet;
	}

	// ------------------------------------------------------------
	public boolean hasMoreElements() {
		// nextSetBit method return -1 if no true bit exist
		return set.busy.nextSetBit(index) != -1;
	}

	// ------------------------------------------------------------
	public String nextElement() {

		index = set.busy.nextSetBit(index);
		return set.elt[index++];
	}
	// ------------------------------------------------------------
}
