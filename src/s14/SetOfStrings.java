package s14;

import java.util.BitSet;

public class SetOfStrings {
	BitSet busy;
	String[] elt;
	int[] total;
	int crtSize;
	static final int DEFAULT_CAPACITY = 5;

	// ------------------------------------------------------------
	public SetOfStrings() {
		this(DEFAULT_CAPACITY);
	}

	public SetOfStrings(int initialCapacity) {
		initialCapacity = Math.max(2, initialCapacity);
		elt = new String[initialCapacity];
		total = new int[initialCapacity];
		busy = new BitSet(initialCapacity);
		crtSize = 0;
	}

	int capacity() {
		return elt.length;
	}

	// Here is the hash function :
	int hashString(String s) {
		int h = s.hashCode() % capacity();
		if (h < 0)
			h = -h;
		return h;
	}

	// PRE: table is not full
	// returns the index where element e is stored, or, if
	// absent, the index of an appropriate free cell
	// where e can be stored
	// return -1 if error occured
	int targetIndex(String e) {
		int valueHashed = hashString(e);
		int totalHashedValue = total[valueHashed];

		do {
			if (busy.get(valueHashed)) {

				// element found
				if (e.equals(elt[valueHashed]))
					return valueHashed;

				// same hash but not same value
				if (hashString(e) == hashString(elt[valueHashed]))
					// have to find the next cell with the value, if no cell with value found, value
					// not in the array
					totalHashedValue--;

			}
			valueHashed++;
			if (valueHashed >= elt.length)
				valueHashed = 0;

		} while (totalHashedValue > 0);

		valueHashed = hashString(e);
		// find the next free cell
		while (busy.get(valueHashed)) {
			valueHashed++;
			if (valueHashed >= elt.length)
				valueHashed = 0;
		}

		return valueHashed;
	}

	public void add(String e) {
		if (crtSize * 2 >= capacity())
			doubleTable();

		if (!contains(e)) {
			int indexValue = targetIndex(e);

			total[hashString(e)]++;
			busy.set(indexValue, true);
			elt[indexValue] = e;
			crtSize++;
		}

	}

	private void doubleTable() {

		SetOfStrings newSet = new SetOfStrings(this.elt.length * 2);
		String[] elements = arrayFromSet();

		for (String str : elements)
			newSet.add(str);

		this.busy = newSet.busy;
		this.total = newSet.total;
		this.elt = newSet.elt;
		this.crtSize = newSet.crtSize;
	}

	public void remove(String e) {
		int i = targetIndex(e);
		if (!busy.get(i))
			return; // elt is absent
		int h = hashString(e);
		total[h]--;
		elt[i] = null;
		busy.clear(i);
		crtSize--;
	}

	public boolean contains(String e) {
		return busy.get(targetIndex(e));
	}

	public void union(SetOfStrings s) {
		String[] fromSet = s.arrayFromSet();

		for (String str : fromSet)
			this.add(str);
	}

	public void intersection(SetOfStrings s) {
		String[] fromSet = s.arrayFromSet();
		SetOfStrings newIntersectionSet = new SetOfStrings();

		for (String str : fromSet) {
			if (this.contains(str))
				newIntersectionSet.add(str);
		}

		this.busy = newIntersectionSet.busy;
		this.total = newIntersectionSet.total;
		this.elt = newIntersectionSet.elt;
		this.crtSize = newIntersectionSet.crtSize;

	}

	public int size() {
		return crtSize;
	}

	public boolean isEmpty() {
		return size() == 0;
	}

	// show all values stored in the array elt whitout empty case
	private String[] arrayFromSet() {
		String[] t = new String[size()];
		int i = 0;
		SetOfStringsItr itr = new SetOfStringsItr(this);
		while (itr.hasMoreElements()) {
			t[i++] = itr.nextElement();
		}
		return t;
	}

	public String toString() {
		SetOfStringsItr itr = new SetOfStringsItr(this);
		if (isEmpty())
			return "{}";
		String r = "{" + itr.nextElement();
		while (itr.hasMoreElements()) {
			r += ", " + itr.nextElement();
		}
		return r + "}";
	}

	// ------------------------------------------------------------
	// tiny example
	// ------------------------------------------------------------
	public static void main(String[] args) {
		String a = "abc";
		String b = "defhijk";
		String c = "hahaha";
		SetOfStrings s = new SetOfStrings();

		s.add(a);
		s.add(b);

		s.remove(a);

		if (s.contains(a) || s.contains(c) || !s.contains(b)) {
			System.out.println("bad news...");
		} else {
			System.out.println("ok");
		}
	}
}
