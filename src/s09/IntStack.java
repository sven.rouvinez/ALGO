package s09;

public class IntStack {
	private static int buf[];
	private static int top;

	public IntStack() {
		this(10);
	}

	public IntStack(int cap) {
		buf = new int[cap];
		top = -1;
	}

	public boolean isEmpty() {
		return top == -1;
	}

	public int top() {
		return buf[top];
	}

	public int pop() {
		int a = buf[top];
		top--;
		return a;
	}

	public void push(int x) {
		checkSize();
		top++;
		buf[top] = x;
	}

	private void checkSize() {
		if (top == buf.length - 1) {
			int[] t = new int[2 * buf.length];
			for (int i = 0; i < buf.length; i++)
				t[i] = buf[i];
			buf = t;
		}
	}

	public static boolean findsBug01() {
		IntStack s = new IntStack(10);
		s.push(0);
		if (s.pop() != 0)
			return true;
		if (!s.isEmpty())
			return true;
		return false;
	}

	public static boolean findsBug02() {
		IntStack s = new IntStack(10);
		s.push(0);
		s.push(1);
		if (s.isEmpty())
			return true;
		return false;
	}

	public static boolean findsBug03() {
		IntStack s = new IntStack(10);
		s.push(0);
		s.push(5);
		s.pop();
		s.push(1);
		if (s.pop() != 1)
			return true;
		if (s.isEmpty())
			return true;
		return false;
	}

	// test with static attributs
	public static boolean findsBugOthers01() {
		IntStack s = new IntStack(10);
		IntStack m = new IntStack(10);

		s.push(0);
		s.push(1);
		s.push(2);

		m.push(3);
		m.push(4);
		m.push(5);

		// the right value of the s.pop is 2
		int valueToTest = s.pop();
		if (valueToTest == 5) {
			return false;
		} else if (valueToTest == 2) {
			return true;
		}

		return true;
	}

	// test with buf[x] = top;
	public static boolean findsBugOthers02() {
		IntStack s = new IntStack(10);

		// this line create an error because of the index
		// s.push(11);

		s.push(4);
		s.push(6);

		// the right value of s.pop is 4
		int valueToTest = s.pop();
		if (valueToTest == 4) {
			return true;
		} else if (valueToTest == 0) {
			return false;
		}

		return true;
	}

	public static void main(String[] args) {
		System.out.println(IntStack.findsBug01());
		System.out.println(IntStack.findsBug02());
		System.out.println(IntStack.findsBug03());
		System.out.println("findsBugOthers01: " + IntStack.findsBugOthers01());
		System.out.println("findsBugOthers02: " + IntStack.findsBugOthers02());
	}
}
