package s09;

import java.util.Arrays;

public class Ex2and3SortingTest {
	// ----- Maybe you'll find that useful for ex. (3)... -------
	@FunctionalInterface
	interface ISorting {
		void sort(int[] t);
	}

	static final ISorting[] algos = { BuggySorting::sort00, BuggySorting::sort01, BuggySorting::sort02,
			BuggySorting::sort03, BuggySorting::sort04, BuggySorting::sort05, BuggySorting::sort06,
			BuggySorting::sort07, BuggySorting::sort08, BuggySorting::sort09, BuggySorting::sort10,
			BuggySorting::sort11 };

	// =============================================================

	public static boolean isSortingResultCorrect(int[] orig, int[] sorted) {

		if (orig.length != sorted.length)
			return false;

		for (int i = 0; i < sorted.length-1; ++i) {

			// test if one monotonie
			if (!(sorted.length >= i + 1) && sorted[i] > sorted[i + 1])
				return false;

			// test if occurences in sorted is strictly equals to occurences in orignal
			if (nbOfOccurrences(sorted, sorted[i]) != nbOfOccurrences(orig, orig[i]))
				return false;
		}

		return true;
	}

	private static int nbOfOccurrences(int[] t, int e) {
		int occurences = 0;

		for (int value : t) {
			if (value == e) {
				occurences++;
			}
		}

		return occurences;
	}

	// ------------------------------------------------------------
	public static void main(String[] args) {

		int[][] testArraysOrig = { { 13, 11, 7, 16, 2, 1, 8, 14, 18, 20, 9, 10, 12, 4, 3, 6, 15, 5, 17, 19 },
				{ 0, 0, 0, 0 }, { 1, 2, 3, 4, 5, 6, 7 }, { 8, 7, 6, 5, 4, 3, 2, 1 }, {} };

		int[][] testArraysSorted = { { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 },
				{ 0, 0, 0, 0 }, { 1, 2, 3, 4, 5, 6, 7 }, { 1, 2, 3, 4, 5, 6, 7, 8 }, {} };

		int sortAlgoNumber = 0;

		for (int i = 0; i < testArraysOrig.length; ++i) {
			System.out.println("Array number: " + i);
			
			for (ISorting a : algos) {

				// copy of original tab because the original is going to be modified by the
				// sorted algorithm
				int[] origToSort = Arrays.copyOf(testArraysOrig[i], testArraysOrig[i].length);
				a.sort(origToSort);

				System.out.println(" Sorting " + (++sortAlgoNumber) + " is: "
						+ isSortingResultCorrect(origToSort, testArraysSorted[i]));
			}
			sortAlgoNumber = 0;
		}
		
	}
}
