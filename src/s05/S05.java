package s05;



public class S05 {

	public static void main(String[] args) {
		System.out.println(factorial(10));
		System.out.println(modulo(11, 3));
		System.out.println(getNumberOf1(13));
		System.out.println("Fibonnaci: " + fibonnaci(4));
		System.out.println(square(8));

	}

	public static int factorial(int n) {

		if (n <= 1)
			return 1;

		return factorial(n - 1) * n;

	}

	public static int modulo(int n, int m) {
		if (n < m)
			return n;
		if (n == m)
			return 0;
		if (n == 1)
			return n;

		return modulo(n - m, m);

	}

	public static int getNumberOf1(int n) {

		if (n <= 1)
			return 1;

		return getNumberOf1(n / 2) + (n % 2);

	}

	public static int fibonnaci(int n) {
		if (n == 0)
			return 0;
		if (n == 1)
			return 1;


		return fibonnaci(n - 1) + fibonnaci(n - 2);

	}

	public static int square(int n) {
		if (n == 1)
			return 1;

		return square(n - 1) + (2 * n - 1);
	}
}
